<?php

namespace App\Http\Controllers;

use App\Mail\FormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class FormController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submit(Request $request)
    {
        $request->validate([
            'companyname' => 'required|string',
            'legalstatus' => ['required', Rule::in(['Eenmanszaak', 'VOF', 'Maatschap', 'Besloten vennootschap', 'Naamloze vennootschap', 'Stichting', 'Vereniging', 'Overig'])],
            'street' => 'required|string',
            'housenumber' => 'required|int',
            'postalcode' => ['required', 'regex:/^[1-9][0-9]{3} ?[a-z]{2}$/i'],
            'city' => 'required|string',
            'iban' => ['required', 'regex:/^[A-Z]{2}[0-9]{2} ?[A-Z]{4} ?[0-9]{10}$/'],
            'emailcustomer' => 'required|email',
            'emailcorrespondence' => 'nullable|email'
        ]);

        Mail::to('verzekering@test.nl')->send(new FormMail($request->all()));

        if(count(Mail::failures()) > 0)
        {
            $message = "Er is iets fout gegaan met het versturen, probeer het later opnieuw";
        }
        else
        {
            $message = "Het formulier is succesvol verzonden";
        }

        return back()->with('mail', $message);
    }
}
