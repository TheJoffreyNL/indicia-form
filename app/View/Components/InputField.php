<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputField extends Component
{

    public $fieldname;
    public $label;

    /**
     * InputField constructor.
     * @param $fieldname
     * @param $label
     */
    public function __construct($fieldname, $label)
    {
        $this->fieldname = $fieldname;
        $this->label = $label;
    }

    /**
     * @return \Closure|\Illuminate\Contracts\Support\Htmlable|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.input-field');
    }
}
