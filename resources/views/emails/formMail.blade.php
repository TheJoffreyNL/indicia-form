<!DOCTYPE html>
<html lang="nl">
    <head>
        <title>Mail van verzekering.nl</title>
    </head>
    <body>
        <h1>Formulier verzending</h1>
        @foreach ($content as $field => $value)
            @if($field != '_token')
                <li>{{ $field }} - {{ $value }}</li>
            @endif
        @endforeach
    </body>
</html>
