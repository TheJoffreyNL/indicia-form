<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Verzekering formulier</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased">
        <div class="flex justify-center items-center h-screen">
            <form method="POST" action="/submit-form" class="w-full max-w-3xl bg-gray-100 rounded-md p-12">
                @csrf
                <x-input-field label="Bedrijfsnaam" fieldname="companyname" />

                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label class="block text-gray-500 font-bold mb-1 md:mb-0 pr-4" for="legalstatus">
                            Rechtsvorm
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <div class="relative">
                            <select name="legalstatus" id="legalstatus" class="block appearance-none w-full bg-gray-200 border {{ ($errors->has('legalstatus')) ? 'border-red-500' : 'border-gray-200' }} text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                <option {{ old('legalstatus') == 'Eenmanszaak' ? 'selected' : '' }}>Eenmanszaak</option>
                                <option {{ old('legalstatus') == 'VOF' ? 'selected' : '' }}>VOF</option>
                                <option {{ old('legalstatus') == 'Maatschap' ? 'selected' : '' }}>Maatschap</option>
                                <option {{ old('legalstatus') == 'Besloten vennootschap' ? 'selected' : '' }}>Besloten vennootschap</option>
                                <option {{ old('legalstatus') == 'Naamloze vennootschap' ? 'selected' : '' }}>Naamloze vennootschap</option>
                                <option {{ old('legalstatus') == 'Stichting' ? 'selected' : '' }}>Stichting</option>
                                <option {{ old('legalstatus') == 'Vereniging' ? 'selected' : '' }}>Vereniging</option>
                                <option {{ old('legalstatus') == 'Overig' ? 'selected' : '' }}>Overig</option>
                            </select>
                            <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                        @if($errors->has('legalstatus'))
                            <div class="text-red-500 text-xs italic">{{ $errors->first('legalstatus') }}</div>
                        @endif
                    </div>
                </div>

                <x-input-field label="Straat" fieldname="street" />

                <x-input-field label="Huisnummer" fieldname="housenumber" />

                <x-input-field label="Postcode" fieldname="postalcode" />

                <x-input-field label="Woonplaats" fieldname="city" />

                <x-input-field label="IBAN" fieldname="iban" />

                <x-input-field label="Email klant" fieldname="emailcustomer" />

                <x-input-field label="Email financiële correspondentie" fieldname="emailcorrespondence" />

                <div class="md:flex md:items-center">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3">
                        <button class="shadow bg-red-400 hover:bg-red-500 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                            Verzenden
                        </button>
                    </div>
                </div>
                @if(session()->has('mail'))
                    <div class="md:flex md:items-center mt-4">
                        <div class="md:w-1/3"></div>
                        <div class="md:w-2/3">
                            {{ session('mail') }}
                        </div>
                    </div>
                @endif
            </form>
        </div>
    </body>
</html>
