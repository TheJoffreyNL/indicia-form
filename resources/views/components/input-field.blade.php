<div class="md:flex md:items-center mb-6">
    <div class="md:w-1/3">
        <label class="block text-gray-500 font-bold mb-1 md:mb-0 pr-4" for="{{ $fieldname }}">
            {{ $label }}
        </label>
    </div>
    <div class="md:w-2/3">
        <input name="{{ $fieldname }}" id="{{ $fieldname }}" type="text" value="{{ old($fieldname) }}" class="bg-gray-200 appearance-none border-2 {{ ($errors->has($fieldname)) ? 'border-red-500' : 'border-gray-200' }} rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-grey-500">
        @if($errors->has($fieldname))
            <div class="text-red-500 text-xs italic">{{ $errors->first($fieldname) }}</div>
        @endif
    </div>
</div>
