<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'email' => 'Dit veld bevat een ongeldig emailadres',
    'in' => 'De gekozen waarde is ongeldig',
    'numeric' => 'Dit veld mag alleen een cijfer bevatten',
    'regex' => 'Dit formaat is ongeldig',
    'required' => 'Dit veld is verplicht',
    'string' => 'Dit veld mag alleen text bevatten',
];
